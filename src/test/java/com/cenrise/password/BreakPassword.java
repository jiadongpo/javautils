package com.cenrise.password;

/**
 * 暴力破解密码之破解数字+字母密码篇
 */
public class BreakPassword {
    //    static String real = "828bf481ef0202145c075c8320f019e2";//真实密码
//    static String real = "b4aec59bc75a4e144f25c1d03efc2cdb";//321
    static String real = "36efdb5d254c768466f362df14a21252";//
    static String pass = "";//执行循环操作找出来的与真实密码相等的字符串
    static String prod = "";//中间产生的字符串
    static String prodTmp = "";//转换之后的字符
//    static String[] s = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};//randomArrays.length

    static String brute = "0123456789" + "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "!@#$%^&amp;*()-=+" + " []\"" + "{}j;': " + ",./&lt;&gt;?'  ";//105

    //定义一个有randomArrays.length个字符串的字符串数组
//生成一个指定位数的密码，每一位有以上randomArrays.length种可能,n=6时，第一个为000000，最后一个为ZZZZZZ,共//randomArrays.length^6种可能。n=7时，第一个为0000000，最后一个为ZZZZZZZ，共randomArrays.length^7种可 能。n=8时，第一个为//00000000，最后一个为ZZZZZZZZ，共randomArrays.length^8种可能
//randomArrays.length^6种情况，累死也列不出来，，，
    static long startTime;

    public static void main(String[] args) throws InterruptedException {
        startTime = System.currentTimeMillis();

        String[] randomArrays = brute.split("");
        System.out.println(getMima(randomArrays, real));


    }

    public static String processStr(String str) {

        String jiami = MD5Utils.encrypt("dong_lu@suixingpay.com", str);
        if (jiami.equals(real)) {
            System.out.println(str);
            long endTime = System.currentTimeMillis();

            System.out.println("程序运行时间：" + (endTime - startTime) + "ms");
            System.exit(0);
        }
        return jiami;
    }

    public static String getMima(String[] randomArrays, String real) {
        //循环遍历出数组中的元素，列出数组中的字符可以组成的所有一位字符串，共randomArrays.length^1种可能
        System.out.println("第1批");
        for (int i = 0; i < randomArrays.length; i++) {
            prod = randomArrays[i];
            prodTmp = processStr(prod);

            if (prodTmp.equals(real)) {
                pass = prod;
                System.out.println("执行" + (i + 1) + "次操作,找到真实密码，为" + pass);

            }

        }
        //列出数组中的字符可以组成的所有两位字符串，共randomArrays.length^2种可能
        System.out.println("第2批");
        if (pass.equals("")) {//在上一级未找到
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    prod = randomArrays[i] + randomArrays[j];
                    prod = processStr(prod);

                    if (prod.equals(real)) {
                        pass = prod;
                        System.out.println("找到真实密码，为" + pass);
                        break;
                    }
                }
            }
        }
        //列出数组中的字符可以组成的所有三位字符串，共randomArrays.length^3种可能
        System.out.println("第3批");
        if (pass.equals("")) {//在上一级未找到
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        prod = randomArrays[i] + randomArrays[j] + randomArrays[k];

                        prod = processStr(prod);

                        if (prod.equals(real)) {
                            pass = prod;
                            System.out.println("找到真实密码，为" + pass);
                            break;
                        }
                    }
                }

            }
        }
        //列出数组中的字符可以组成的所有4位字符串，共randomArrays.length^4种可能
        System.out.println("第4批");
        if (pass.equals("")) {//在上一级未找到
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l];
                            prod = processStr(prod);
                            if (prod.equals(real)) {
                                pass = prod;
                                System.out.println("找到真实密码，为" + pass);
                                break;
                            }
                        }
                    }
                }
            }
        }
        //列出数组中的字符可以组成的所有5位字符串，共randomArrays.length^5种可能
        System.out.println("第5批");
        if (pass.equals("")) {//在上一级未找到
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m];
                                prod = processStr(prod);
                                if (prod.equals(real)) {
                                    pass = prod;
                                    System.out.println("找到真实密码，为" + pass);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        System.out.println("第6批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        if (pass.equals("")) {//在上一级未找到
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n];
                                    prod = processStr(prod);
                                    if (prod.equals(real)) {
                                        pass = prod;
                                        System.out.println("找到真实密码，为" + pass);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }


        System.out.println("第7批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o];
                                        prod = processStr(prod);
                                        if (prod.equals(real)) {
                                            pass = prod;
                                            System.out.println("找到真实密码，为" + pass);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第8批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p];
                                            prod = processStr(prod);
                                            if (prod.equals(real)) {
                                                pass = prod;
                                                System.out.println("找到真实密码，为" + pass);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第9批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q];
                                                prod = processStr(prod);
                                                if (prod.equals(real)) {
                                                    pass = prod;
                                                    System.out.println("找到真实密码，为" + pass);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第10批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r];
                                                    prod = processStr(prod);
                                                    if (prod.equals(real)) {
                                                        pass = prod;
                                                        System.out.println("找到真实密码，为" + pass);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第10批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s];
                                                        prod = processStr(prod);
                                                        if (prod.equals(real)) {
                                                            pass = prod;
                                                            System.out.println("找到真实密码，为" + pass);
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第11批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t];
                                                            prod = processStr(prod);
                                                            if (prod.equals(real)) {
                                                                pass = prod;
                                                                System.out.println("找到真实密码，为" + pass);
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第11批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u];
                                                                prod = processStr(prod);
                                                                if (prod.equals(real)) {
                                                                    pass = prod;
                                                                    System.out.println("找到真实密码，为" + pass);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第12批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                for (int v = 0; v < randomArrays.length; v++) {
                                                                    prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u] + randomArrays[v];
                                                                    prod = processStr(prod);
                                                                    if (prod.equals(real)) {
                                                                        pass = prod;
                                                                        System.out.println("找到真实密码，为" + pass);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第12批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals("")) {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                for (int v = 0; v < randomArrays.length; v++) {
                                                                    for (int w = 0; w < randomArrays.length; w++) {
                                                                        prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u] + randomArrays[v] + randomArrays[w];
                                                                        prod = processStr(prod);
                                                                        if (prod.equals(real)) {
                                                                            pass = prod;
                                                                            System.out.println("找到真实密码，为" + pass);
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        System.out.println("第13批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals(""))

        {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                for (int v = 0; v < randomArrays.length; v++) {
                                                                    for (int w = 0; w < randomArrays.length; w++) {
                                                                        for (int x = 0; x < randomArrays.length; x++) {
                                                                            prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u] + randomArrays[v] + randomArrays[w] + randomArrays[x];
                                                                            prod = processStr(prod);
                                                                            if (prod.equals(real)) {
                                                                                pass = prod;
                                                                                System.out.println("找到真实密码，为" + pass);
                                                                                break;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

            }
        }

        System.out.println("第14批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals(""))

        {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                for (int v = 0; v < randomArrays.length; v++) {
                                                                    for (int w = 0; w < randomArrays.length; w++) {
                                                                        for (int x = 0; x < randomArrays.length; x++) {
                                                                            for (int y = 0; y < randomArrays.length; y++) {
                                                                                prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u] + randomArrays[v] + randomArrays[w] + randomArrays[x] + randomArrays[y];
                                                                                prod = processStr(prod);
                                                                                if (prod.equals(real)) {
                                                                                    pass = prod;
                                                                                    System.out.println("找到真实密码，为" + pass);
                                                                                    break;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        System.out.println("第15批");
        //列出数组中的字符可以组成的所有6位字符串，共randomArrays.length^6种可能
        //在上一级未找到
        if (pass.equals(""))

        {
            for (int i = 0; i < randomArrays.length; i++) {
                for (int j = 0; j < randomArrays.length; j++) {
                    for (int k = 0; k < randomArrays.length; k++) {
                        for (int l = 0; l < randomArrays.length; l++) {
                            for (int m = 0; m < randomArrays.length; m++) {
                                for (int n = 0; n < randomArrays.length; n++) {
                                    for (int o = 0; o < randomArrays.length; o++) {
                                        for (int p = 0; p < randomArrays.length; p++) {
                                            for (int q = 0; q < randomArrays.length; q++) {
                                                for (int r = 0; r < randomArrays.length; r++) {
                                                    for (int s = 0; s < randomArrays.length; s++) {
                                                        for (int t = 0; t < randomArrays.length; t++) {
                                                            for (int u = 0; u < randomArrays.length; u++) {
                                                                for (int v = 0; v < randomArrays.length; v++) {
                                                                    for (int w = 0; w < randomArrays.length; w++) {
                                                                        for (int x = 0; x < randomArrays.length; x++) {
                                                                            for (int y = 0; y < randomArrays.length; y++) {
                                                                                for (int z = 0; z < randomArrays.length; z++) {
                                                                                    prod = randomArrays[i] + randomArrays[j] + randomArrays[k] + randomArrays[l] + randomArrays[m] + randomArrays[n] + randomArrays[o] + randomArrays[p] + randomArrays[q] + randomArrays[r] + randomArrays[s] + randomArrays[t] + randomArrays[u] + randomArrays[v] + randomArrays[w] + randomArrays[x] + randomArrays[y] + randomArrays[z];
                                                                                    prod = processStr(prod);
                                                                                    if (prod.equals(real)) {
                                                                                        pass = prod;
                                                                                        System.out.println("找到真实密码，为" + pass);
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        return pass;
    }


}
