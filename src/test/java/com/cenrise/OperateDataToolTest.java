package com.cenrise;

import com.cenrise.utils.Dbutil;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class OperateDataToolTest {

    private static Logger logger = LoggerFactory.getLogger(OperateDataToolTest.class);
    String sql = "CREATE OR REPLACE PROCEDURE DCM_OWNER.CHUANGXIN_ORG_DAY_PROC(ADT IN STRING) IS\n" +
            "BEGIN\n" +
            "  DELETE FROM DCM_OWNER.T_CHUANGXIN_ORG_DAY WHERE RUN_DATE = ADT;\n" +
            "  COMMIT;\n" +
            "\n" +
            "  INSERT INTO DCM_OWNER.T_CHUANGXIN_ORG_DAY\n" +
            "    SELECT J.TRAN_DT RUN_DATE,\n" +
            "           B.ORG_NAME,\n" +
            "           B.ORG_NO,\n" +
            "           SUM(J.TXN_AMTWZS) AMTWZS,\n" +
            "           SUM(J.TXN_COTWZS) COTWZS,\n" +
            "           SUM(J.TXN_AMTZFS) AMTZFS,\n" +
            "           SUM(J.TXN_COTZFS) COTZFS,\n" +
            "           SUM(J.TXN_AMTQQS) AMTQQS,\n" +
            "           SUM(J.TXN_COTQQS) COTQQS,\n" +
            "           SUM(J.TXN_AMTWZF) AMTWZF,\n" +
            "           SUM(J.TXN_COTWZF) COTWZF,\n" +
            "           SUM(J.TXN_AMTZFF) AMTZFF,\n" +
            "           SUM(J.TXN_COTZFF) COTZFF,\n" +
            "           SUM(J.TXN_AMTQQF) AMTQQF,\n" +
            "           SUM(J.TXN_COTQQF) COTQQF,\n" +
            "           SUM(J.TXN_AMTWZP) AMTWZP,\n" +
            "           SUM(J.TXN_COTWZP) COTWZP,\n" +
            "           SUM(J.TXN_AMTZFP) AMTZFP,\n" +
            "           SUM(J.TXN_COTZFP) COTZFP,\n" +
            "           SUM(J.TXN_AMTQQP) AMTQQP,\n" +
            "           SUM(J.TXN_COTQQP) COTQQP,\n" +
            "           B.channel_name\n" +
            "      FROM (SELECT DISTINCT B1.ORG_NO, B1.ORG_NAME, R.TRADE_MNO,m.channel_name\n" +
            "              FROM DCM_OWNER.TRS_QR_APP_RESO_INFO R\n" +
            "              LEFT JOIN DCM_OWNER.TRS_QR_ORG_APP_INFO B1\n" +
            "                ON R.ORG_CODE = B1.ORG_NO\n" +
            "LEFT JOIN dcm_owner.trs_qr_app_channel_repo_info  f  ON  r.mno=f.mno AND r.org_code=f.org_code \n" +
            "LEFT JOIN dcm_owner.TRS_QR_CHANNEL_MANAGE m ON  f.channel_no=m.channel_no\n" +
            "WHERE R.STATUS = '01'   and   b1.STATUS = '01' ) B\n" +
            "INNER JOIN (SELECT ORSC_MNO,\n" +
            "                        T.TRAN_DT,\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTWZS, --微信成功金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTWZS, --微信成功笔数\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTZFS, --支付宝成功金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTZFS, --支付宝成功笔数\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTQQS, --QQ钱包成功金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('N', 'D', 'R') AND\n" +
            "                                   TEX.FINISH_DT = ADT THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTQQS, --QQ钱包成功笔数\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTWZF, --微信失败金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTWZF, --微信失败笔数\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTZFF, --支付宝失败金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTZFF, --支付宝失败笔数\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTQQF, --QQ钱包失败金额\n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_STS != 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTQQF, --QQ钱包失败笔数\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTWZP, --微信未支付金额\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q11', 'Q12') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTWZP, --微信未支付笔数\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTZFP, --支付宝未支付金额\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q01', 'Q02') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTZFP, --支付宝未支付笔数\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               TRAN_AMT\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -TRAN_AMT\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_AMTQQP, --QQ钱包未支付金额\n" +
            "                        \n" +
            "                        SUM(CASE\n" +
            "                              WHEN TRAN_CD IN ('999.88.025', '999.88.028','999.88.125') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               1\n" +
            "                              WHEN TRAN_CD IN ('10120025', '10130025') AND\n" +
            "                                   T.IN_MOD IN ('Q31', 'Q32') AND\n" +
            "                                   T.TRAN_FLG IN ('L') and T.TRAN_STS = 'S' THEN\n" +
            "                               -1\n" +
            "                              ELSE\n" +
            "                               0\n" +
            "                            END) AS TXN_COTQQP --QQ钱包未支付笔数\n" +
            "                   FROM DCM_OWNER.T_PTS_TRANDATA T\n" +
            "                   LEFT JOIN DCM_OWNER.T_PTS_TRANDATA_EXTEND TEX\n" +
            "                     ON T.TRAN_DT = TEX.TRAN_DT\n" +
            "                    AND TEX.UUID = T.UUID\n" +
            "                  WHERE T.TRAN_DT = ADT\n" +
            "                    AND T.TRAN_CD IN\n" +
            "                        ('10120025', '999.88.028', '10130025', '999.88.025','999.88.125')\n" +
            "                  GROUP BY ORSC_MNO, T.TRAN_DT) J\n" +
            "        ON B.TRADE_MNO=J.ORSC_MNO -----外部资源编号\n" +
            "    \n" +
            "     GROUP BY TRAN_DT, B.ORG_NO, B.ORG_NAME, B.channel_name;\n" +
            "COMMIT;\n" +
            "  INSERT INTO DCM_OWNER.T_MONITOR_PROC_TABLE_LOG\n" +
            "  VALUES\n" +
            "    ('DCM_OWNER',\n" +
            "     'CHUANGXIN_ORG_DAY_PROC',\n" +
            "     'DCM_OWNER',\n" +
            "     'T_CHUANGXIN_ORG_DAY',\n" +
            "     TO_CHAR(SYSDATE, 'yyyymmddhh24miss'),\n" +
            "     ADT);\n" +
            "  COMMIT;\n" +
            "END;";

    @Test
    public void exeSQL() {
        Dbutil dbutil = new Dbutil();

        logger.info("非url更改数据库操作开始");
        int updataRs = -100;
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            conn = dbutil.getConnection();
            st = conn.createStatement();
            //执行SQL
            boolean flag = st.execute(sql);
            String rStr = "执行失败";
            if (updataRs >= 0) {
                rStr = "执行成功";
            }
        } catch (Exception e) {
            e.printStackTrace();
//            throw e;
        } finally {
            //释放资源

        }
        logger.info("非url更改数据库操作结束");

    }


}